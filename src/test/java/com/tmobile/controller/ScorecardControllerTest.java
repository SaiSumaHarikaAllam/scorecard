package com.tmobile.controller;

import com.tmobile.entity.ItemMaster;
import com.tmobile.entity.Scorecard;
import com.tmobile.service.ScorecardService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ScorecardControllerTest {

    @InjectMocks
    ScorecardController scorecardController;

    @Mock
    ScorecardService scorecardService;

    @Test
    public void saveScorecardTest(){
        List<Scorecard> scorecardList= new ArrayList<Scorecard>();
        Scorecard scorecard = new Scorecard();
        scorecard.setResponse("response");
        scorecard.setComment("comment");
        scorecard.setCreatedDate("createdDate");
        scorecard.setUpdatedDate("updatedDate");
        scorecard.setCreatedBy("createdBy");
        scorecard.setUpdatedBy("updatedBy");
        scorecardList.add(scorecard);
        scorecardService.saveScorecard(scorecardList);
        ResponseEntity<String> response = scorecardController.insertScorecardDetails(scorecardList);
        assertTrue(response.getStatusCode() == HttpStatus.OK);
        assertTrue((response.getBody()) != null);
    }
    @Test
    public void testGetScorecardBySprint() throws Exception {
        List<Scorecard> scorecardList= new ArrayList<Scorecard>();
        Scorecard scorecard = new Scorecard();
        scorecard.setResponse("response");
        scorecardList.add(scorecard);
        when(scorecardService.getScorecardBySprint(1L,1L)).thenReturn(scorecardList);
        ResponseEntity<List<Scorecard>> res = scorecardController.getScorecardBySprint(1L,1L);
        assertTrue(res.getBody().get(0).getResponse().equals(scorecardList.get(0).getResponse()));
    }
    @Test
    public void testGetScorecardByItem() throws Exception {
        List<ItemMaster> itemMasterList= new ArrayList<ItemMaster>();
        ItemMaster itemMaster = new ItemMaster();
        itemMaster.setItem("item");
        itemMasterList.add(itemMaster);
        when(scorecardService.getScorecardItemMaster()).thenReturn(itemMasterList);
        ResponseEntity<List<ItemMaster>> res = scorecardController.getScorecardItemMaster();
        assertTrue(res.getBody().get(0).getItem().equals(itemMasterList.get(0).getItem()));
    }
}