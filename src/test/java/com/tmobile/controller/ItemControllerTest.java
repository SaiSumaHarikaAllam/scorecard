package com.tmobile.controller;

import com.tmobile.entity.ItemMaster;
import com.tmobile.service.ItemService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ItemControllerTest {
    @InjectMocks
    ItemController itemController;

    @Mock
    ItemService itemService;

    @Test
    public void insertItemTest() throws Exception {
        ItemMaster itemDetails = new ItemMaster();
        itemDetails.setStatus("status");
        itemService.saveItemDetails(itemDetails);
        ResponseEntity<String> res = itemController.insertItemDetails(itemDetails);
        assertTrue(res.getBody().equals("Record inserted succesfully"));
    }

    @Test
    public void getResponseByIdTest() throws Exception {
        ItemMaster itemDetails = new ItemMaster();
        itemDetails.setItem("item");
        Mockito.when(itemService.getItemById(1L)).thenReturn(itemDetails);
        ResponseEntity<ItemMaster> res = itemController.findItemById(1L);
        assertTrue(res.getBody().getItem().equals("item"));
    }
    @Test
    public void updateItemTest() throws Exception {
        ItemMaster itemDetails = new ItemMaster();
        itemDetails.setStatus("status");
        itemService.updateItemDetails(itemDetails);
        ResponseEntity<String> res = itemController.updateItemDetails(itemDetails);
        assertTrue(res.getBody().equals("Record updated successfully"));
    }

}