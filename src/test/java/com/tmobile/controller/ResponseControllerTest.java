package com.tmobile.controller;

import com.tmobile.entity.ResponseMaster;
import com.tmobile.service.ResponseService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
class ResponseControllerTest {

    @InjectMocks
    ResponseController responseController;

    @Mock
    ResponseService responseService;

    @Test
    public void insertResponseDetailsTest() throws Exception {
        ResponseMaster response = new ResponseMaster();
        response.setStatus("status");
        responseService.saveResponseDetails(response);
        ResponseEntity<String> res = responseController.insertResponseDetails(response);
        assertTrue(res.getBody().equals("Record inserted succesfully"));
    }

    @Test
    public void getResponseByIdTest() throws Exception {
        ResponseMaster response = new ResponseMaster();
        response.setResponse("response");
        Mockito.when(responseService.getResponseById(1L)).thenReturn(response);
        ResponseEntity<ResponseMaster> res = responseController.findResponseById(1L);
        assertTrue(res.getBody().getResponse().equals("response"));
    }
    @Test
    public void updateResponseDetailsTest() throws Exception {
        ResponseMaster response = new ResponseMaster();
        response.setStatus("status");
        responseService.updateResponseDetails(response);
        ResponseEntity<String> res = responseController.updateResponseDetails(response);
        assertTrue(res.getBody().equals("Record updated successfully"));
    }

}