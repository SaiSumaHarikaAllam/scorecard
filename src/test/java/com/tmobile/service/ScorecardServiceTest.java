package com.tmobile.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import com.tmobile.entity.ItemMaster;
import com.tmobile.entity.ResponseMaster;
import com.tmobile.entity.Scorecard;
import com.tmobile.repository.ItemMasterRepository;
import com.tmobile.repository.ScorecardRepository;

@ExtendWith(MockitoExtension.class)
class ScorecardServiceTest {

    @InjectMocks
    ScorecardService scorecardService;

    @Mock
    ScorecardRepository scorecardRepository;

    @Mock
    ItemMasterRepository itemMasterRepository;

    @Test
    public void saveScorecardDetailsTest(){
        List<Scorecard> scorecardList = new ArrayList<Scorecard>();
        scorecardService.saveScorecard(scorecardList);
    }
    @Test
    public void getScorecardBySprintTest() throws Exception {
        Optional<List<Scorecard>> response = Optional.of(new ArrayList<>());
        Scorecard scorecard = new Scorecard();
        scorecard.setResponse("response");
        response.get().add(scorecard);
        Mockito.when(scorecardRepository.findBySprintIdAndProjectId(1L,1L)).thenReturn(response);
        List<Scorecard> result = scorecardService.getScorecardBySprint(1L,1L);
        assertTrue("response".equals(scorecard.getResponse()));
    }
    @Test
    public void getScorecardBySprintExceptionTest() throws Exception {
        Optional<Scorecard> response;
        try{
            List<Scorecard> res = scorecardService.getScorecardBySprint(1L,1L);
        }catch (Exception ex){
            assertNotNull(ex.getMessage());
        }
    }
    @Test
    public void getScorecardItemMasterTest() {
        //  Optional<ResponseMaster> responseDetails = Optional.of(new ResponseMaster());
        List<ItemMaster> itemResponse= new ArrayList<ItemMaster>();
        ItemMaster itemDetails = new ItemMaster();

        ResponseMaster response = new ResponseMaster();
        response.setResponse("response");
        List<ResponseMaster> responseList = new ArrayList<ResponseMaster>();
        responseList.add(response);
        itemDetails.setCategory("category");
        itemDetails.setItem("item");
        itemDetails.setItemId(1L);
        itemDetails.setCreatedBy("createdby");
        itemDetails.setSlno(1L);
        itemDetails.setResponse(responseList);
        itemDetails.setFocusArea("focusArea");
        itemDetails.setCreatedDate("createdDate");
        itemDetails.setUpdatedDate("updatedDate");
        itemDetails.setUpdatedBy("updatedBy");
        itemDetails.setStatus("status");
        itemResponse.add(itemDetails);
        List<ItemMaster> result = scorecardService.getScorecardItemMaster();
        //Mockito.when(itemMasterRepository.findAll()).thenReturn(itemResponse);
        assertTrue("item".equals(itemDetails.getItem()));
    }

}