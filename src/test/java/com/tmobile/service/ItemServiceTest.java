package com.tmobile.service;

import com.tmobile.entity.ItemMaster;
import com.tmobile.repository.ItemMasterRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ItemServiceTest {

    @InjectMocks
    ItemService itemService;

    @Mock
    ItemMasterRepository itemMasterRepository;

    @Test
    public void saveItemDetailsTest(){
        ItemMaster itemDetails = new ItemMaster();
        itemService.saveItemDetails(itemDetails);
    }


    @Test
    public void getItemByIdTest() {
        Optional<ItemMaster> response = Optional.of(new ItemMaster());
        response.get().setItem("item");
        response.get().setCategory("category");
        response.get().setItemId(1L);
        response.get().setFocusArea("focusArea");
        Mockito.when(itemMasterRepository.findById(1L)).thenReturn(response);
        ItemMaster item = itemService.getItemById(1L);
        assertTrue("item".equals(item.getItem()));
    }

    @Test
    public void updateItemDetailsTest(){
        ItemMaster itemDetails = new ItemMaster();
        itemService.updateItemDetails(itemDetails);
    }
}