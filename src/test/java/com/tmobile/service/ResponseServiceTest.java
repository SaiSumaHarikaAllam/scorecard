package com.tmobile.service;

import com.tmobile.entity.ItemMaster;
import com.tmobile.entity.ResponseMaster;
import com.tmobile.repository.ResponseRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ResponseServiceTest {

    @InjectMocks
    ResponseService responseService;

    @Mock
    ResponseRepository responseRepository;

    @Test
    public void saveResponseDetailsTest(){
        ResponseMaster responseDetails = new ResponseMaster();
        responseService.saveResponseDetails(responseDetails);
    }


    @Test
    public void getResponseByIdTest() {
        Optional<ResponseMaster> responseDetails = Optional.of(new ResponseMaster());
        responseDetails.get().setResponse("response");
        responseDetails.get().setResponseId(1L);
        Mockito.when(responseRepository.findById(1L)).thenReturn(responseDetails);
        ResponseMaster response = responseService.getResponseById(1L);
        assertTrue("response".equals(response.getResponse()));
    }

    @Test
    public void updateResponseDetailsTest(){
        ResponseMaster responseDetails = new ResponseMaster();
        responseService.updateResponseDetails(responseDetails);
    }

}