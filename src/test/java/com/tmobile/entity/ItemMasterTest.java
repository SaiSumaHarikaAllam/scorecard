package com.tmobile.entity;

import org.assertj.core.internal.ErrorMessages;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
class ItemMasterTest {

    @Test
    public void test(){
        ItemMaster itemMaster = new ItemMaster();
        itemMaster.setItemId(1L);
        itemMaster.setItem("item");
        itemMaster.setCategory("category");
        itemMaster.setSlno(1L);
        itemMaster.setCreatedBy("createdBy");
        itemMaster.setUpdatedBy("updatedBy");
        itemMaster.setFocusArea("focusArea");
    }
    @Test
    public void testToString() {
        ItemMaster item = new ItemMaster();
        String result = "ItemMaster(itemId=null, slno=null, item=null, response=null, category=null, focusArea=null, createdDate=null, updatedDate=null, createdBy=null, updatedBy=null, status=null)";
        assertEquals(item.toString(), result);
    }

    @Test
    public void testHashcode() {
        ItemMaster p1 = new ItemMaster();
        ItemMaster p2 = new ItemMaster();
        Map<ItemMaster, String> map = new HashMap<>();
        map.put(p1, "dummy");
        assertEquals("dummy", map.get(p2));
    }

}