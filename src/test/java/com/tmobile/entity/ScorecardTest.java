package com.tmobile.entity;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ScorecardTest {
    @Test
    public void test(){
        Scorecard scorecard = new Scorecard();
        scorecard.setResponseId(1L);
        scorecard.setSprintId(1L);
        scorecard.setProjectId(1L);
        scorecard.setScorecard_Id(1L);
        scorecard.setStatus("status");
        scorecard.setResponseId(1L);
        scorecard.setItemId(1L);
        scorecard.setRating(1F);
        scorecard.setResponse("response");
        scorecard.setComment("comment");
        scorecard.setCreatedDate("createdDate");
        scorecard.setUpdatedDate("updatedDate");
        scorecard.setCreatedBy("createdBy");
        scorecard.setUpdatedBy("updatedBy");
    }
    @Test
    public void testToString() {
        Scorecard scorecardResponse = new Scorecard();
        String result = "Scorecard(scorecard_Id=null, projectId=null, sprintId=null, itemId=null, responseId=null, response=null, comment=null, rating=null, createdDate=null, updatedDate=null, createdBy=null, updatedBy=null, status=null)";
        assertEquals(scorecardResponse.toString(), result);
    }

    @Test
    public void testHashcode() {
        Scorecard p1 = new Scorecard();
        Scorecard p2 = new Scorecard();
        Map<Scorecard, String> map = new HashMap<>();
        map.put(p1, "dummy");
        assertEquals("dummy", map.get(p2));
    }

}