package com.tmobile.entity;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ResponseMasterTest {
    @Test
    public void test(){
        ResponseMaster responseMaster = new ResponseMaster();
        responseMaster.setResponseId(1L);
        responseMaster.setResponse("response");
        responseMaster.setCreatedDate("createdDate");
        responseMaster.setUpdatedDate("updatedDate");
        responseMaster.setCreatedBy("createdBy");
        responseMaster.setUpdatedBy("updatedBy");
    }
    @Test
    public void testToString() {
        ResponseMaster response = new ResponseMaster();
        String result = "ResponseMaster(responseId=null, itemId=null, response=null, rating=null, createdDate=null, updatedDate=null, createdBy=null, updatedBy=null, status=null)";
        assertEquals(response.toString(), result);
    }

    @Test
    public void testHashcode() {
        ResponseMaster p1 = new ResponseMaster();
        ResponseMaster p2 = new ResponseMaster();
        Map<ResponseMaster, String> map = new HashMap<>();
        map.put(p1, "dummy");
        assertEquals("dummy", map.get(p2));
    }

}