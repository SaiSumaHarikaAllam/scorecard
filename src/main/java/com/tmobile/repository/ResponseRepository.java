package com.tmobile.repository;

import com.tmobile.entity.ResponseMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
    public interface ResponseRepository extends JpaRepository<ResponseMaster,Long> {
}
