package com.tmobile.repository;

import com.tmobile.entity.ItemMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ItemMasterRepository extends JpaRepository<ItemMaster, Long> {
	//Optional<ItemMaster> findByItemId(Long itemId);

}
