package com.tmobile.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tmobile.entity.Scorecard;

import java.util.List;
import java.util.Optional;

@Repository
public interface ScorecardRepository extends JpaRepository<Scorecard,Long> {

    Optional<List<Scorecard>> findBySprintIdAndProjectId(Long sprinId, Long projectId);


}
