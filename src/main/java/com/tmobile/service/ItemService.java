package com.tmobile.service;

import com.tmobile.entity.ItemMaster;
import com.tmobile.repository.ItemMasterRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class ItemService {

    @Autowired
    ItemMasterRepository itemRepository;

    public void saveItemDetails(ItemMaster itemdetails) {
        ItemMaster itemdata = new ItemMaster();
        BeanUtils.copyProperties(itemdetails,itemdata);
        itemRepository.save(itemdata);
    }
    public ItemMaster getItemById(Long id) {
        Optional<ItemMaster> item = itemRepository.findById(id);
        ItemMaster itemDetails = new ItemMaster();
        itemDetails.setItemId(item.get().getItemId());
        return item.get();
    }
    public void updateItemDetails(ItemMaster itemDetails) {
        ItemMaster item = new ItemMaster();
        BeanUtils.copyProperties(itemDetails,item);
        itemRepository.save(item);
    }
}
