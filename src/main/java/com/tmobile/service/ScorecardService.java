package com.tmobile.service;

import java.util.List;
import java.util.Optional;

import com.tmobile.entity.ItemMaster;
import com.tmobile.repository.ItemMasterRepository;
import com.tmobile.repository.ResponseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmobile.entity.Scorecard;
import com.tmobile.repository.ScorecardRepository;

@Service
public class ScorecardService {
    @Autowired
    ScorecardRepository scorecardRepository;

    @Autowired
    ResponseRepository responseRepository;

    @Autowired
    ItemMasterRepository itemmastRepository;

    public void saveScorecard(List<Scorecard> scorecard) {
        scorecardRepository.saveAll(scorecard);
    }

    public List<Scorecard> getScorecardBySprint(Long projectID, Long sprintID) throws Exception {
        Optional<List<Scorecard>> scorecard = scorecardRepository.findBySprintIdAndProjectId(projectID, sprintID);

        if (scorecard.isPresent()) {
            return scorecard.get();
        } else {
            throw new Exception("No details found");
        }
    }

    public List<ItemMaster> getScorecardItemMaster() {
        List<ItemMaster> response = itemmastRepository.findAll();
        return response;
    }

}


