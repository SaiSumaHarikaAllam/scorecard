package com.tmobile.service;

import com.tmobile.entity.ResponseMaster;
import com.tmobile.repository.ResponseRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class ResponseService {

    @Autowired
    ResponseRepository responseRepository;

    public void saveResponseDetails(ResponseMaster responsedetails) {
        ResponseMaster responsedata = new ResponseMaster();
        BeanUtils.copyProperties(responsedetails,responsedata);
        responseRepository.save(responsedata);
    }
    public ResponseMaster getResponseById(Long id) {
        Optional<ResponseMaster> response = responseRepository.findById(id);
        ResponseMaster responseDetails = new ResponseMaster();
        responseDetails.setResponseId(response.get().getResponseId());
        return response.get();

    }
    public void updateResponseDetails(ResponseMaster responseDetails) {
        ResponseMaster response = new ResponseMaster();
        BeanUtils.copyProperties(responseDetails,response);
        responseRepository.save(response);
    }
}
