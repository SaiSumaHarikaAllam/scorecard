package com.tmobile.controller;

import com.tmobile.entity.ItemMaster;
import com.tmobile.service.ItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/item")
@CrossOrigin(methods = { RequestMethod.GET,
        RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT })
@Api(tags = "Item Controller", description = "Create and retrieve items")
public class ItemController {
    @Autowired
    ItemService itemservice;

    @GetMapping("/itemById/{id}")
    @ApiOperation(value = "Get item by Id", notes = "Fetches item by particular Id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = ItemMaster.class)})
    public ResponseEntity<ItemMaster> findItemById(@PathVariable long id) {
        ItemMaster itemdata = itemservice.getItemById(id);
        return new ResponseEntity<>(
                itemdata,
                HttpStatus.OK);
    }

    @PostMapping("/save")
    @ApiOperation(value = "Save item", notes = "Saves item details")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Record inserted successfully")})
    public ResponseEntity<String> insertItemDetails(@RequestBody ItemMaster itemDetails) {
        itemservice.saveItemDetails(itemDetails);
        return new ResponseEntity<>(
                "Record inserted succesfully",
                HttpStatus.OK);
    }

    @PutMapping("/update")
    @ApiOperation(value = "Updates item", notes = "Updates item details")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Record updated successfully")})
    public ResponseEntity<String> updateItemDetails(@RequestBody ItemMaster itemDetails) {
        itemservice.updateItemDetails(itemDetails);
        return new ResponseEntity<>(
                "Record updated successfully",
                HttpStatus.OK);
    }
}
