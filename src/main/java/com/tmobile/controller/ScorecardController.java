package com.tmobile.controller;

import com.tmobile.entity.ItemMaster;
import com.tmobile.entity.Scorecard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.tmobile.service.ScorecardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;

@RestController
@RequestMapping("/scorecard")
@CrossOrigin(methods = {RequestMethod.GET,
        RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
@Api(tags = "ScoreCard Controller", description = "Create and retrieve scorecards")
public class ScorecardController {

    @Autowired
    ScorecardService service;
    @PostMapping("/save")
    @ApiOperation(value = "Save score card", notes = "Saves a list of scorecard")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Record inserted successfully")})
    public ResponseEntity<String> insertScorecardDetails(@RequestBody List<Scorecard> scorecard) {
        service.saveScorecard(scorecard);
        return new ResponseEntity<>(
                "Record inserted successfully",
                HttpStatus.OK);
    }

    @GetMapping(path = "/get-scorecard/{sprintId}/{projectId}")
    @ApiOperation(value = "Get scorecard for particular sprintId and project id", notes = "Get scorecard for particular sprintId and project id")
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "OK", response = Scorecard.class, responseContainer = "List")})
    public ResponseEntity<List<Scorecard>> getScorecardBySprint(@PathVariable("sprintId") Long sprintId,
                                                                @PathVariable("projectId") Long projectId) throws Exception {
        List<Scorecard> response = service.getScorecardBySprint(sprintId, projectId);
        return new ResponseEntity<List<Scorecard>>(response, HttpStatus.OK);
    }

    @GetMapping("/getAllMasterItem")
    @ApiOperation(value = "Gets All ItemMaster", notes = "Get all list of ItemMaster")
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "OK", response = ItemMaster.class, responseContainer = "List")})
    public ResponseEntity<List<ItemMaster>> getScorecardItemMaster() {
        List<ItemMaster> response = service.getScorecardItemMaster();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}