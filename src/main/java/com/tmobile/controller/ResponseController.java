package com.tmobile.controller;


import com.tmobile.entity.ResponseMaster;
import com.tmobile.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/response")
@CrossOrigin( methods = { RequestMethod.GET,
        RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT })
@Api(tags = "Response Controller", description = "Create and retrieve responses")
public class ResponseController {

    @Autowired
    ResponseService responseService;

    @GetMapping("/responseById/{id}")
    @ApiOperation(value = "Get response by Id", notes = "Fetches response by particular Id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = ResponseMaster.class)})
    public ResponseEntity<ResponseMaster> findResponseById(@PathVariable long id) {
        ResponseMaster responsedata = responseService.getResponseById(id);
        return new ResponseEntity<>(
                responsedata,
                HttpStatus.OK);    }

    @PostMapping("/save")
    @ApiOperation(value = "Save response detail", notes = "Saves responses details")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Record inserted successfully")})
    public ResponseEntity<String>  insertResponseDetails(@RequestBody ResponseMaster responseDetails) {
        responseService.saveResponseDetails(responseDetails);
        return new ResponseEntity<>(
                "Record inserted succesfully",
                HttpStatus.OK);

    }
    @PutMapping("/update")
    @ApiOperation(value = "Updates response detail", notes = "Updates responses details")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Record updated successfully")})
    public ResponseEntity<String> updateResponseDetails(@RequestBody ResponseMaster responseDetails) {
        responseService.updateResponseDetails(responseDetails);
        return new ResponseEntity<>(
                "Record updated successfully",
                HttpStatus.OK);
    }
}
