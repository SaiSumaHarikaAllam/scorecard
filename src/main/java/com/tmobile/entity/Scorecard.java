package com.tmobile.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="Sprint_Score_Card")
public class Scorecard {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long scorecard_Id;

    @Column(name="Project_ID")
    private Long projectId;

    @Column(name="Sprint_ID")
    private Long sprintId;

    @Column(name="item_Id")
    private Long itemId ;

    @Column(name="Response_ID")
    private Long responseId ;

    @Column(name="Response")
    private String response ;

    @Column(name="Comment")
    private String comment ;

    @Column(name="Rating")
    private Float rating ;

    @Column(name = "created_Date")
    private String createdDate;

    @Column(name = "update_Date")
    private String updatedDate;

    @Column(name = "created_By")
    private String createdBy;

    @Column(name = "updated_By")
    private String updatedBy;

    @Column(name = "status")
    private String status;

}
