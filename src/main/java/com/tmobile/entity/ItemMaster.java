package com.tmobile.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name="Item_Master_Entity")
public class ItemMaster  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long itemId;

    @Column(name = "sl_No")
    private Long slno;

    @Column(name = "Item")
    private String item;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "itemId", fetch = FetchType.EAGER ,targetEntity = ResponseMaster.class)
    @JsonIgnoreProperties(value = {"itemId"}, allowSetters=true)
    private List<ResponseMaster> response;

    @Column(name = "Category")
    private String category;

    @Column(name = "focus_Area")
    private String focusArea;

    @Column(name = "created_Date")
    private String createdDate;

    @Column(name = "update_Date")
    private String updatedDate;

    @Column(name = "created_By")
    private String createdBy;

    @Column(name = "updated_By")
    private String updatedBy;

    @Column(name = "status")
    private String status;

}



