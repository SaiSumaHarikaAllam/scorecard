package com.tmobile.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "Response_Master_Entity")
public class ResponseMaster  {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long responseId;

	// Mapping the column of this table
	@ManyToOne
	@JoinColumn(name = "itemId")
	@JsonIgnoreProperties(value = {"response"}, allowSetters=true)
	ItemMaster itemId;

	@Column(name = "Response")
	private String response;

	@Column(name = "rating")
	private Float rating;

	@Column(name = "created_Date")
	private String createdDate;

	@Column(name = "update_Date")
	private String updatedDate;

	@Column(name = "created_By")
	private String createdBy;

	@Column(name = "updated_By")
	private String updatedBy;

	@Column(name = "status")
	private String status;

}
